const http = require('http')
const fs = require('fs')
const uuid = require('uuid')


const server = http.createServer((req, res) => {

    const urlPath = req.url
    const splittedPath = urlPath.split('/')
    let checkPath = splittedPath[1]

    if (checkPath === 'html')
        renderHtml(res)

    else if (checkPath === 'json')
        renderJson(res)

    else if (checkPath === 'uuid')
        renderUuid(res)

    else if (checkPath === 'status')
        renderStatus(res, splittedPath)

    else if (checkPath === 'delay')
        renderDelay(res, splittedPath)

    else {
        res.writeHead(404, { 'Content-Type': 'text/plain' })
        res.end('Not found')
    }
})

const renderHtml = (res) => {

    fs.readFile('index.html', 'utf8', (err, data) => {
        if (err) {
            console.log(err)
            internalServerError(res)
        }

        res.writeHead(200, { 'Content-Type': 'text/html' })
        res.end(data)
    })
}

const renderJson = (res) => {

    fs.readFile('index.json', 'utf8', (err, data) => {
        if (err) {
            console.log(err)
            internalServerError(res)
        }

        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(data)
    })
}

const renderUuid = (res) => {

    try {
        const uniqueId = uuid.v4()
        let returnValue = {
            'uuid': uniqueId
        }

        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(returnValue))
    }
    catch (err) {
        console.log(err)
        internalServerError(res)
    }
}

const renderStatus = (res, splittedPath) => {

    try {
        const sCode = splittedPath[2]

        if (sCode !== undefined) {

            res.writeHead(200, { 'Content-Type': 'text/plain' })
            res.end(`Response with status code ${sCode}`)
        }
        else {
            res.writeHead(404, { 'Content-Type': 'text/plain' })
            res.end(`Not found`)
        }
    }
    catch (err) {
        console.log(err)
        internalServerError(res)
    }
}

const renderDelay = (res, splittedPath) => {

    try {
        const timeDelayString = splittedPath[2]
        const timeDelay = parseInt(timeDelayString)
        sendDelayedResponse(timeDelay, res)
    }
    catch {
        internalServerError(res)
    }
}

const sendDelayedResponse = (timeDelay, res) => {

    setTimeout(() => {

        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end(`Delayed response after ${timeDelay} seconds`)

    }, timeDelay * 1000)
}

const internalServerError = (res) => {

    res.writeHead(500, { 'Content-Type': 'text/plain' })
    res.end('Internal Server Error')
}

server.listen(8001, () => console.log('Server Started'))